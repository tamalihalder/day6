public class Employee{
    String name;
    String department;
    int age;
    public Employee() {
        this("John Doe");
    }
    public  Employee(String name){
        this(name, "Finance" );
    }
    public Employee(String name, String department){
        this(name, department, 35 );
    }
    public Employee(String name, String department, int age){
        this.name=name;
        this.department=department;
        this.age=age;
    }
    public void showDetails(){
        System.out.println("Name is " + name +" Department is "+ department +" age is "+age);
    }

    public static void main(String[] args) {
        Employee employee=new Employee();
        employee.showDetails();
    }
}
